---
date: 2015-04-15
title: Publicat l'Okular 0.22
---
S'ha publicat la versió 0.22 de l'Okular conjuntament amb la publicació 15.04 de les Aplicacions del KDE. L'Okular 0.22 és una actualització recomanada per a tothom que usi l'Okular.
