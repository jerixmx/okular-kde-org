---
date: 2019-12-12
title: Publicat l'Okular 1.9
---
S'ha publicat la versió 1.9 de l'Okular. Aquesta publicació presenta el suport per als arxius de ComicBook «cb7», un suport millorat de JavaScript per als fitxers PDF, i altres correccions i millores secundàries. Podeu revisar el registre complet de canvis a <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. L'Okular 1.9 és una actualització recomanada per a tothom que usi l'Okular.
