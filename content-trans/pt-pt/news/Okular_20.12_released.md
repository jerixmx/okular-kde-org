---
date: 2020-12-10
title: O Okular 20.12 foi lançado
---
A versão 20.12 do Okular foi lançada. Esta versão introduz o deslocamento cinético, melhoras na gestão de páginas, melhoria na interface para dispositivos móveis e diversas correcções e funcionalidades pequenas em todo o lado. Poderá verificar o registo de alterações completo em <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
