---
date: 2009-12-01
title: O Okular 0.9.4 foi lançado
---
A quarta versão de manutenção da série KDE 4.3 inclui o Okular 0.9.4. Inclui algumas correcções de estoiros e algumas pequenas correcções de erros na interface. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
