---
date: 2016-12-15
title: O Okular 1.0 foi lançado
---
A versão 1.0 do Okular foi lançada em conjunto com a versão 16.12 das Aplicações do KDE. Esta versão baseia-se agora nas Plataformas do KDE 5, podendo verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
