---
date: 2009-03-04
title: O Okular 0.8.1 foi lançado
---
A primeira versão de manutenção da série KDE 4.2 inclui o Okular 0.8.1. Inclui alguns estoiros nas infra-estruturas para CHM e DjVu, assim como algumas pequenas correcções na interface do utilizador. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
