---
date: 2010-08-10
title: Publicouse Okular 0.11
---
A versión 0.11 de Okular publicouse xunto coa versión 4.5 das aplicacións de KDE. Esta versión introduce pequenas correccións e funcionalidades e recoméndase para calquera que use Okular.
