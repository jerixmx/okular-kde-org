---
date: 2013-08-16
title: Publicouse Okular 0.17
---
A versión 0.17 de Okular publicouse xunto coa versión 4.11 das aplicacións de KDE. Esta versión introduce novas funcionalidades como a de desfacer e refacer en formularios e anotacións e a posibilidade de configurar as ferramentas de revisión. Okular 0.17 é unha actualización recomendada para calquera que use Okular.
