---
date: 2008-09-03
title: Publicouse Okular 0.7.1
---
A primeira versión de mantemento da serie 4.1 de KDE inclúe 0.7.1. Inclúe algunhas correccións de quebras e outras pequenas correccións. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
