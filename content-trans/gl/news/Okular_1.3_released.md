---
date: 2017-12-14
title: Publicouse Okular 1.3
---
A versión 1.3 de Okular publicouse coa versión 17.12 das aplicacións de KDE. Esta versión introduce cambios na forma de gardas as anotacións e no funcionamento dos datos de formulario, permite actualizacións de renderización parciais para ficheiros que tardan moito en renderizarse, fai interactivas as ligazóns de texto no modo de selección de texto, engade unha opción de «Compartir» ao menú de «Ficheiro», engade compatibilidade con Markdown e soluciona algúns dos problemas de compatibilidade con HiDPI. Pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 é unha actualización recomendada para calquera que use Okular.
