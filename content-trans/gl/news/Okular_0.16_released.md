---
date: 2013-02-06
title: Publicouse Okular 0.16
---
A versión 0.16 de Okular publicouse xunto coa versión 4.10 das aplicacións de KDE. Esta versión introduce novas funcionalidades como a posibilidade de ampliar a maiores niveis en documentos PDF, un visor baseado en Active para dispositivos de tableta, maior compatibilidade con filmes de PDF, o campo de vista segue a selección, e melloras na edición de anotacións.Okular 0.16 é unha actualización recomendada para calquera que use Okular.
