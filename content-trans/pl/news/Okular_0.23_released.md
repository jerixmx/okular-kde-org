---
date: 2015-08-19
title: Okular 0.23 wydany
---
Wersja 0.23 Okulara została wydana wraz z wydaniem Aplikacji do KDE 15.08. Wydanie to wprowadza przejścia z zanikaniem w trybie prezentacji, a także poprawki dotyczące przypisów i odtwarzania filmów. Okular 0.23 jest zalecanym uaktualnieniem dla wszystkich go używających.
