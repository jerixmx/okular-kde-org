---
date: 2008-02-05
title: Okular 0.6.1 wydany
---
Pierwsze wydanie podtrzymujące linię KDE 4.0 obejmuje Okular 0.6.1. Zawiera sporo poprawek błędów, w tym ulepszone nieponawianie pobierania plików podczas zapisywania, usprawnienia użytkowania, itp. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
