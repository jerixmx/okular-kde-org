---
date: 2008-10-03
title: Okular 0.7.2 wydany
---
Drugie wydanie podtrzymujące linię KDE 4.1 obejmuje Okular 0.7.2. Zawiera ono drobne poprawki w modułach TIFF i Comicbook oraz zmienia domyślny poziom powiększenia na "dopasowany do szerokości". Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
