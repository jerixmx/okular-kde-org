---
date: 2008-02-05
title: O Okular 0.6.1 foi lançado
---
A primeira versão de manutenção da série KDE 4.0 inclui o Okular 0.6.1. Ela incorpora algumas pequenas correções de erros, incluindo a correção na transferência sucessiva de arquivos durante o salvamento, melhoria na usabilidade, etc. Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
