---
date: 2015-12-16
title: O Okular 0.24 foi lançado
---
A versão 0.24 do Okular foi lançada junto com a versão 15.12 do KDE Applications. Esta versão introduz pequenas correções de erros e novas funcionalidades, e você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. É uma atualização recomendada a todos os usuários do Okular.
