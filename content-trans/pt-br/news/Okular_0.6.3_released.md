---
date: 2008-04-02
title: O Okular 0.6.3 foi lançado
---
A terceira versão de manutenção da série KDE 4.0 inclui o Okular 0.6.3. Ela incorpora algumas correções de erros, por exemplo, uma melhor maneira de obter a posição do texto em um documento PDF, assim como algumas correções no sistema de anotações e no índice. Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
