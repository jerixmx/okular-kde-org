---
date: 2019-08-15
title: O Okular 1.8 foi lançado
---
A versão 1.8 do Okular foi lançada junto com o KDE Applications versão 19.08. Esta versão introduz o recurso de configurar o fim de linha da anotação entre outras correções e melhorias. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. O Okular 1.8 é uma atualização recomendada a todos os usuários do Okular.
