---
date: 2019-12-12
title: O Okular 1.9 foi lançado
---
A versão 1.9 do Okular foi lançada. Esta versão introduz o suporte ao cb7 para arquivos ComicBook, melhorias no suporte ao JavaScript para arquivos PDF entre várias outras correções e melhorias. Você pode verificar o registro de alterações completo em <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. O Okular 1.9 é uma atualização recomendada a todos os usuários do Okular.
