---
date: 2010-08-10
title: Okular 0.11 lansat
---
Versiunea 0.11 a Okular a fost lansată împreună cu lansarea aplicațiilor KDE 4.5. Această versiune introduce mici corecții și caracteristici și este o actualizare recomandată pentru toți cei care folosesc Okular.
