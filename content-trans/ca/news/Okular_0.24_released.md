---
date: 2015-12-16
title: Publicat l'Okular 0.24
---
S'ha publicat la versió 0.24 de l'Okular conjuntament amb la publicació 15.12 de les Aplicacions del KDE. Aquesta publicació presenta petites esmenes d'errors i millores. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. L'Okular 0.24 és una actualització recomanada per a tothom que usi l'Okular.
