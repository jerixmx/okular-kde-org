---
date: 2016-12-15
title: Okular 1.0 출시
---
Okular 1.0 버전은 KDE 프로그램 16.12와 함께 출시되었습니다. 이 릴리스부터는 KDE 프레임워크 5를 사용합니다. 전체 변경 내역을 확인하려면 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a> 페이지를 방문하십시오. Okular 1.0은 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.
