---
menu:
  main:
    parent: about
    weight: 4
title: Kontakt
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

Sie können mit dem Okular-Team auf verschiedene Art und Weise in Kontakt treten:

* Mailingliste: Um die Entwicklung von Okular zu koordinieren, wird die [Mailingliste okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) von kde.org benutzt. Sie können sie verwenden, um über die Entwicklung der Kernanwendung zu diskutieren und Rückmeldungen zu existierenden oder neuen Anzeigemodulen sind gern gesehen.

* IRC: Als Chat werden die Kanäle [#okular](irc://irc.kde.org/#okular) und [#kde-devel](irc://irc.kde.org/#kde-devel) im [Freenode-Netzwerk](http://www.freenode.net/) benutzt. Dort können Sie mit einigen der Okular-Entwickler sprechen.

* Matrix: den vorher genannten Chat erreichen Sie auch im Matrix-Netzwerk unter [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum: Möchten Sie lieber ein Forum benutzen, gehen Sie zum [Okular-Forum](http://forum.kde.org/viewforum.php?f=251), ein Teil der vielen [KDE-Community-Foren](http://forum.kde.org/).

* Fehler und Wünsche können im [KDE-Fehlerverfolgungssystem](http://bugs.kde.org) berichtet werden. Möchten Sie mitmachen, dann starten Sie mit der Liste der wichtigsten [Fehler](https://community.kde.org/Okular).
