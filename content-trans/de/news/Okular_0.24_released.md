---
date: 2015-12-16
title: Okular 0.24 veröffentlicht
---
Die Version 0.24 von Okular wurde zusammen mit den KDE-Anwendungen 15.12 veröffentlicht. Diese Veröffentlichung enthält kleinere Fehlerkorrekturen und neue Funktionen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
