---
date: 2008-09-03
title: Okular 0.7.1 veröffentlicht
---
Die erste Wartungsversion der Reihe KDE 4.1 enthält Okular 0.7.1. Diese Version bringt Korrekturen, die Abstürze verhindern und behebt ein paar weitere Fehler.Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">hier</a>.
