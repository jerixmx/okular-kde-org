---
date: 2017-04-20
title: Okular 1.1 veröffentlicht
---
Die Version 1.1 von Okular wurde zusammen mit den KDE-Anwendungen 17.04 veröffentlicht. In dieser Veröffentlichung kann die Größe von Anmerkungen geändert werden, automatische Berechnungen in Formularen werden unterstützt, es gibt Verbesserungen für Touchscreens und weitere Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.1 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
