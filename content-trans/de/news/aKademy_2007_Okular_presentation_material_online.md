---
date: 2007-07-10
title: Okular-Präsentationsmaterial der aKademy 2007 ist online
---
Der Okular-Vortrag, den Pino Toscano auf der <a href="http://akademy2007.kde.org">aKademy 2007</a> gehalten hat, ist jetzt verfügbar. Es gibt <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">Folien</a> und das<a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">Video</a>.
