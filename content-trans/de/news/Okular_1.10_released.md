---
date: 2020-04-23
title: Okular 1.10 veröffentlicht
---
Die Version 1.10 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält kinetisches Blättern, Verbesserungen bei der Verwaltung von Unterfenstern und der Mobil-Oberfläche sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
