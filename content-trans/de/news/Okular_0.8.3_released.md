---
date: 2009-05-06
title: Okular 0.8.3 veröffentlicht
---
Die dritte Wartungsversion der Reihe KDE 4.2 enthält Okular 0.8.3. Diese Version bringt nicht viele Neuigkeiten für Okular. Die einzige erwähnenswerte Änderung ist die Verbesserung der Thread-Sicherheit beim Generieren der Seiten-Abbilder von XPS-Dokumenten. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">hier</a>.
