---
date: 2020-12-10
title: Okular 20.12 veröffentlicht
---
Die Version 20.12 von Okular wurde veröffentlicht. Diese Veröffentlichung enthält einige Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
