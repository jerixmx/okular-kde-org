---
date: 2008-04-02
title: Okular 0.6.3 yayımlandı
---
KDE 4.0 serisinin üçüncü bakım sürümü Okular 0.6.3'ü içeriyor. Bu sürüm PDF belgelerinde metin konumu almak için daha iyi bir yol bulunması gibi birkaç hata düzeltmesi, not sisteminde ve içerik tablosunda çeşitli düzeltmeler içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a> adresinde okuyabilirsiniz.
