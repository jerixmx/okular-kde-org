---
date: 2009-04-02
title: Okular 0.8.2 yayımlandı
---
KDE 4.2 serisinin ikinci bakım sürümü Okular 0.8.2'yi içeriyor. Bu sürüm DVI ve pdfsync için daha iyi (çalışmasının umulduğu) ters arama desteği, düzeltmeler ve sunum kipiyle ilgili küçük iyileştirmeler içeriyor. Düzeltilen tüm durumları <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a> adresinde okuyabilirsiniz.
