---
date: 2009-05-06
title: Izšel je Okular 0.8.3
---
Tretja izdaja vzdrževanja serije KDE 4.2 vključuje Okular 0.8.3. Za Okular ne ponuja veliko novic, edina pomembna sprememba je nit z več varnosti pri ustvarjanju slik strani XPS dokumenta. Vse odpravljene težave si lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
