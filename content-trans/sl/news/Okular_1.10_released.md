---
date: 2020-04-23
title: Izšel je Okular 1.10
---
Izšla je različica programa Okular 1.10. Ta izdaja uvaja kinetično pomikanje, izboljšave upravljanja zavihkov in izboljšave uporabniškega vmesnika na mobilnih napravah in različni manjši popravki in funkcije povsod. Celoten dnevnik sprememb lahko preverite na <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
