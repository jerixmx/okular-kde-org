---
date: 2008-03-05
title: Izšel je Okular 0.6.2
---
Druga izdaja za vzdrževanje serije KDE 4.0 vključuje Okular 0.6.2. Vključuje kar nekaj popravkov napak, vključno z večjo stabilnostjo pri zapiranju dokumenta in majhne popravke za sistem zaznamkov. Vse odpravljene težave lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
