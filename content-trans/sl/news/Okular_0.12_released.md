---
date: 2011-01-26
title: Izšel je Okular 0.12
---
Različica 0.12 programa Okular je bila izdana skupaj z izdajo programov KDE 4.6. Ta izdaja predstavlja majhne popravke in funkcije in je priporočena posodobitev za vse, ki uporabljajo Okular.
