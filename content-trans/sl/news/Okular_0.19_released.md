---
date: 2014-04-16
title: Izšel je Okular 0.19
---
Različica Okular 0.19 je bila izdana skupaj s programi KDE verzije 4.13. Ta izdaja uvaja nove zmožnosti, kot je podpora zavihkov v uporabniškem vmesniku, uporaba zaslona DPI, tako da se velikost strani ujema z resnično velikostjo papirja, izboljšave okvira za razveljavitev/uveljavitev in druge zmožnosti/izboljšave. Okular 0.19 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
