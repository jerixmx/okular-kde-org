---
date: 2017-12-14
title: Izšel je Okular 1.3
---
Različica Okular 1.3 je bila izdana skupaj s KDE Applications 17.12. Ta izdaja uvaja spremembe v načinu shranjevanja zaznamkov in delovanju podatkov obrazcev, podpira delne posodobitve upodabljanja za datoteke, ki se dolgo upodabljajo, naredi besedilne povezave interaktivne v načinu za izbiro besedila, dodana je možnost skupne rabe v meniju datoteke, dodana podpora za Markdown in odpravila nekatere težave s podporo za HiDPI. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. Okular 1.3 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
