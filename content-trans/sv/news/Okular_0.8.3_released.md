---
date: 2009-05-06
title: Okular 0.8.3 utgiven
---
Den tredje underhållsutgåvan i KDE 4.2-serien inkluderar Okular 0.8.3. Den innehåller inte mycket nytt för Okular. Den enda relevanta ändringen är att gå fram försiktigare när sidbilder skapas av XPS-dokument. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
