---
date: 2006-08-27
title: Okular 0.5 instabil förutgåva utgiven
---
Okular-gruppen presenterar med stolthet förutgåvan av Okular som går att kompilera med <a href="http://dot.kde.org/1155935483/">förutgåvan av KDE 4, 'Krash'</a>. Förutgåvan har inte ännu fullständig funktionalitet, eftersom vi har mycket att polera och slutföra, men prova den gärna och ge så mycket återmatning som du vill. Paketet med förutgåvan finns på <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Ta en titt på <a href="download.php">nerladdningssidan</a> för att försäkra dig om att du har biblioteken som krävs.
