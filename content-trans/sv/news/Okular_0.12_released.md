---
date: 2011-01-26
title: Okular 0.12 utgiven
---
Version 0.12 av Okular har givits ut tillsammans med utgåva 4.6 av KDE:s program. Utgåvan introducerar mindre rättelser och funktioner och är en rekommenderad uppdatering för alla som använder Okular.
