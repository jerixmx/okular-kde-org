---
intro: Okular stöder ett stort antal dokumentformat och användarfall. Den här sidan
  hänvisar alltid till den stabila serien av Okular, för närvarande Okular 20.12.
layout: formats
menu:
  main:
    name: Dokumentformat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status för hantering av dokumentformat
---
