---
date: 2011-07-27
title: Megjelent az Okular 0.13
---
Az Okular 0.13-as kiadása a KDE Applications 4.7-tel együtt jelent meg. Ebben a kiadásban kisebb javítások és új funkciók szerepelnek, a frissítés minden Okular felhasználónak ajánlott.
