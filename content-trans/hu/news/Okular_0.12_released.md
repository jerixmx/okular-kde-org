---
date: 2011-01-26
title: Megjelent az Okular 0.12
---
Az Okular 0.12-es kiadása a KDE Applications 4.6-tal együtt jelent meg. Ebben a kiadásban kisebb javítások és új funkciók szerepelnek, a frissítés minden Okular felhasználónak ajánlott.
