---
date: 2018-08-16
title: Κυκλοφορία της έκδοσης 1.5 του Okular
---
Η έκδοση 1.5 του Okular κυκλοφόρησε μαζί με την έκδοση 18.08 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει βελτιώσεις στις φόρμες ανάμεσα σε άλλες διορθώσεις και χαρακτηριστικά. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.5.
