---
date: 2008-09-03
title: Κυκλοφορία της έκδοσης 0.7.1 του Okular
---
Η πρώτη έκδοση συντήρησης της σειράς 4.1 του KDE περιλαμβάνει το Okular 0.7.1 με κάποιες διορθώσεις κατάρρευσης μεταξύ άλλων ήσσονος σημασίας διορθώσεων. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
