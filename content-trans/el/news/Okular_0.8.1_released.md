---
date: 2009-03-04
title: Κυκλοφορία της έκδοσης 0.8.1 του Okular
---
Η πρώτη έκδοση συντήρησης της σειράς 4.2 του KDE περιλαμβάνει το Okular 0.9.4 με κάποιες διορθώσεις κατάρρευσης στα συστήματα υποστήριξης των CHM και DjVu και διορθώσεις ήσσονος σημασίας στη διεπαφή. Μπορείτε να διαβάσετε για όλα τα διορθωμένα προβλήματα στο <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
