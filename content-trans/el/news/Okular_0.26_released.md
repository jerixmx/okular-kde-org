---
date: 2016-08-18
title: Κυκλοφορία της έκδοσης 0.26 του Okular
---
Η έκδοση 0.26 του Okular κυκλοφόρησε μαζί με την έκδοση 16.08 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει πολύ μικρές αλλαγές και μπορείτε να δείτε την πλήρη καταγραφή τους στο <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 0.26.
