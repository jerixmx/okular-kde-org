---
date: 2019-12-12
title: Κυκλοφορία της έκδοσης 1.9 του Okular
---
Η έκδοση 1.9 του Okular κυκλοφόρησε. Η έκδοση αυτή εισάγει υποστήριξη cb7 για αρχειοθήκες ComicBook και βελτιωμένη υποστήριξη JavaScript για PDF αρχεία μεταξύ διαφόρων διορθώσεων και χαρακτηριστικών. Μπορείτε να δείτε την πλήρη καταγραφή αλλαγών στο <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.9.
