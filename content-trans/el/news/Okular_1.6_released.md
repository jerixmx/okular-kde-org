---
date: 2018-12-13
title: Κυκλοφορία της έκδοσης 1.6 του Okular
---
Η έκδοση 1.6 του Okular κυκλοφόρησε μαζί με την έκδοση 18.12 του πακέτου KDE Applications. Η έκδοση αυτή εισάγει το νέο εργαλείο σχολιασμού γραφομηχανής ανάμεσα σε άλλες διορθώσεις και χαρακτηριστικά. Μπορείτε να δείτε την πλήρη καταγραφή των αλλαγών στο <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Συνιστάται σε όσους χρησιμοποιούν το Okular να κάνουν την ενημέρωση στην έκδοση 1.6.
