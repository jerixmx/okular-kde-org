---
date: 2015-12-16
title: Okular 0.24 vrijgegeven
---
De 0.24 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 15.12. Deze vrijgave introduceert kleine reparaties van bugs en functies, u kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 wordt aanbevolen aan iedereen die Okular gebruikt.
