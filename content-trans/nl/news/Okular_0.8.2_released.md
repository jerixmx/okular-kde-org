---
date: 2009-04-02
title: Okular 0.8.2 vrijgegeven
---
De tweede onderhoudsuitgave van de KDE 4.2 seriee bevat Okular 0.8.2. Het bevat betere ondersteuning (hopelijk werkend) voor DVI en "pdfsync inverse" zoeken en reparaties en kleine verbeteringen in de presentatiemodus. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>
