---
date: 2008-09-03
title: Okular 0.7.1 vrijgegeven
---
De eerste onderhoudsuitgave van de KDE 4.1 serie bevat Okular 0.7.1. Het bevat reparaties van enige crashes en een paar kleine reparaties. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
