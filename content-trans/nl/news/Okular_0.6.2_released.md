---
date: 2008-03-05
title: Okular 0.6.2 vrijgegeven
---
De tweede onderhoudsuitgave van de KDE 4.0 serie bevat Okular 0.6.2. Het bevat reparaties van enige bugs, met meer stabiliteit bij het sluiten van document en kleine reparaties aan het systeem voor bladwijzers. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
