---
date: 2008-02-05
title: Выпущен Okular 0.6.1
---
Первый выпуск KDE 4.0 включает в себя Okular 0.6.1. В Okular 0.6.1 было сделано довольно много исправлений, включая отсутствие необходимости перезагружать файлы при сохранении, улучшения удобства использования и так далее. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
