---
date: 2008-09-03
title: Выпущен Okular 0.7.1
---
Первый выпуск KDE 4.1 включает в себя Okular 0.7.1. Okular 0.7.1 включает в себя некоторые исправления ошибок, приводящих к аварийному завершению программы, а также другие мелкие исправления. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
