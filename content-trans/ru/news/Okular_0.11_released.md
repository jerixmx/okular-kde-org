---
date: 2010-08-10
title: Выпущен Okular 0.11
---
Версия Okular 0.11 была выпущена вместе с другими приложениями KDE 4.5. В этот выпуск были включены небольшие исправления, а также добавлены новые функции. Мы рекомендуем всем, кто использует Okular, обновиться до новой версии.
