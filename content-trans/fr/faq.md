---
faq:
- answer: Les paquets pour Ubuntu (aussi bien que de Kubuntu) sont compilés sans la
    prise en charge de ces deux formats. La raison est fournie dans [ce](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    rapport de Launchpad.
  question: Lorsque j'utilise Ubuntu, je ne peux pas lire des documents de type « CHM »
    ou « EPub », même en ayant installé les bibliothèque « okular-extra-backends »
    et « libchm1 ». Pourquoi ?
- answer: Parce que vous n'avez pas d'outils de synthèse vocale sur votre système.
    Installez la bibliothèque « Qt Speech » et ils devraient s'activer.
  question: Pourquoi les options de synthèse vocale dans le menu « Outils » sont elles
    grisées ?
- answer: Veuillez installer le paquet « poppler-data »
  question: Certains caractères ne sont pas rendus et quand vous activez le mode de
    débogage, certaines lignes mentionnent « Paquet de langues manquants pour xxx »
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Questions fréquemment posées
---
