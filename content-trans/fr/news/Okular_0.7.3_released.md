---
date: 2008-11-04
title: Publication de la version 0.7.3 d'Okular
---
La version 0.7.3 d'Okular a été livrée avec la troisième version de maintenance de KDE 4.1. Elle ne corrige que quelques problèmes mineurs dans l'interface utilisateur et dans la recherche de texte. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
