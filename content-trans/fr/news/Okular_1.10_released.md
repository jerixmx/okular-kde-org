---
date: 2020-04-23
title: Publication de la version 1.10 d'Okular
---
La version 1.10 d'Okular a été livrée. Cette version intègre le défilement cinétique, des améliorations pour la gestion des ongles, des améliorations pour l'interface utilisateur en version mobile ainsi que des corrections et fonctionnalités mineures. Vous pouvez consulter la liste complète des changements à cette adresse : <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. La version 1.10 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 
