---
date: 2008-09-03
title: Publication de la version 0.7.1 d'Okular
---
La version 0.7.1 d'Okular a été livrée avec la première version de maintenance de KDE 4.1. Elle corrige quelques plantages en plus d'autres problèmes mineurs. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
