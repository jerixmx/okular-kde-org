---
date: 2006-11-02
title: Publication de la version de développement instable 0.5.81 d'Okular
---
L'équipe d'Okular est fière de vous annoncer la livraison d'une première pré version d'Okular correspondant à la <a href="http://dot.kde.org/1162475911/">deuxième pré version des développeurs de KDE 4</a>. Cette pré version n'est pas encore totalement fonctionnelle puisqu'il reste beaucoup de choses à affiner et à terminer. Mais, vous êtes libre de la tester et de de nous fournir autant de retours que vous pouvez. Vous pouvez trouver le paquet de la pré version à <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Vous pouvez aussi regarder à la page de<a href="download.php">téléchargement</a> pour être sûr que vous possédez toutes les bibliothèques nécessaires.
