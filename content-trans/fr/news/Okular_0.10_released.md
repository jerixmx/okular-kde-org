---
date: 2010-02-09
title: Publication de la version 0.10 d'Okular
---
La version 0.10 d'Okular a été livrée avec les applications fournies avec la version 4.4 SC de KDE. En plus d'améliorations générales concernant la stabilité, cette version apporte la prise en charge nouvelle ou améliorée des recherches en avant ou en arrière en faisant le lien entre les lignes de code source LaTeX avec les emplacements correspondants dans les fichiers « DVI » ou « pdf ».
