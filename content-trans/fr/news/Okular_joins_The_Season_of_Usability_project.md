---
date: 2007-01-31
title: 'Okular rejoint le projet « The Season of Usability » '
---
L'équipe d'Okular est fière de vous annoncer que Okular est l'une des applications sélectionnées pour participer au projet <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, mis en œuvre par des experts en ergonomie à <a href="http://www.openusability.org">OpenUsability</a>. A ce jour, nous nous réjouissons d'accueillir Sharad Baliyan dans l'équipe et nous remercions Florian Graessle et Pino Toscano pour leurs travaux soutenus pour améliorer Okular.
