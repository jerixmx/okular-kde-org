---
menu:
  main:
    parent: about
    weight: 4
title: Contact
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/>

Vous pouvez contacter l'équipe d'Okular de plusieurs façons :

* Liste de discussions : pour coordonner le développement de Okular, la [liste de discussion « okular-devel » ](https://mail.kde.org/mailman/listinfo/okular-devel) sur kde.org est utilisée. Vous pouvez y discuter du développement du cœur de l'application et du retour d'expérience concernant les moteurs existants ou nouveaux est apprécié.

* IRC : pour une discussion générale, utilisez l'IRC [#okular](irc://irc.kde.org/#okular) et [#kde-devel](irc://irc.kde.org/#kde-devel) sur le [réseau « Freenode » ](http://www.freenode.net/). Certains développeurs d'Okular peuvent y être trouvés.

* Matrix : le salon mentionné peut être aussi atteint sur le réseau Matrix grâce à [#okular:kde.org](https://matrix.to/#/#okular:kde.org).

* Forum : si vous préférez utiliser un forum, vous pouvez utiliser le [forum d'Okular](http://forum.kde.org/viewforum.php?f=251) parmi les plus importants [forums de la communauté KDE](http://forum.kde.org/).

* Bogues et souhaits : ils doivent être signalés dans le [gestionnaire de bogues de KDE](http://bugs.kde.org). Si vous voulez contribuer, vous pouvez trouver une liste des bogues principaux [ici](https://community.kde.org/Okular).
