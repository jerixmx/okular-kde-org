---
date: 2021-04-22
title: Okular 21.04 publicado
---
La versión 21.04 de Okular ha sido publicada. Esta versión introduce el firmado digital de archivos PDF, así como varias correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelogs/releases/21.04.0#okular'>https://kde.org/announcements/changelogs/releases/21.04.0#okular</a>. Okular 21.04 es una actualización recomendada para todos los usuarios de Okular.
