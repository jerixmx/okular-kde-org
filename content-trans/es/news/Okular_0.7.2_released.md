---
date: 2008-10-03
title: Okular 0.7.2 publicado
---
El segundo lanzamiento de mantenimiento de la serie KDE 4.1 incluye Okular 0.7.2, que contiene varias correcciones de errores menores en los motores TIFF y Comicbook, y el cambio a «Ajustar al ancho» como nivel de ampliación por omisión. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
