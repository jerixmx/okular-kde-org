---
date: 2008-02-05
title: Okular 0.6.1 publicado
---
La primera versión de mantenimiento de la serie KDE 4.0 incluye Okular 0.6.1, que contiene varias correcciones de errores (entre ellas, no volver a descargar archivos cuando se guardan), mejoras de usabilidad, etc. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
