---
date: 2008-03-05
title: Okular 0.6.2 publicado
---
La segunda versión de mantenimiento de la serie KDE 4.0 incluye Okular 0.6.2, que contiene varias correcciones de errores (entre ellas, una mayor estabilidad cuando se cierra un documento) y alguna mejora en el sistema de marcadores. Puede ver los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
