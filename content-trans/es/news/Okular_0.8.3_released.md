---
date: 2009-05-06
title: Okular 0.8.3 publicado
---
El tercer lanzamiento de la serie KDE 4.2 incluye Okular 0.8.3. No hay mucho de nuevo en Okular. El único cambio relevante es mayor estabilidad al generar imágenes de páginas en un documento XPS.Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
