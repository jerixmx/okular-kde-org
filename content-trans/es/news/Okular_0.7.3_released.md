---
date: 2008-11-04
title: Okular 0.7.3 publicado
---
El tercer lanzamiento de mantenimiento de la serie KDE 4.1 incluye Okular 0.7.3, que contiene cambios menores en la interfaz de usuario y en la búsqueda de texto. Puede ver todos los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
