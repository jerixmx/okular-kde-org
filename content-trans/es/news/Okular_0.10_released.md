---
date: 2010-02-09
title: Okular 0.10 publicado
---
La versión 0.10 de Okular ha sido publicada junto con KDE 4.4. Además de mejoras generales en estabilidad, esta versión tiene soporte mejorado para búsqueda, enlazando líneas de código fuente LaTeX con las ubicaciones correspondientes en archivos dvi y pdf.
