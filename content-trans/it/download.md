---
intro: Okular è disponibile come pacchetto precompilato per un'ampia gamma di sistemi.
  Sulla destra puoi verificare lo stato dei pacchetti per la tua distribuzione Linux,
  oppure continuare a leggere le informazioni sugli altri sistemi operativi
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Okular è già disponibile nella maggior parte delle distribuzioni Linux. Puoi
    installarlo dal [KDE Software Center](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logo di Flatpak
  name: Flatpak
  text: Puoi installare l'ultima versione [Flatpak di Okular](https://flathub.org/apps/details/org.kde.okular)
    da Flathub. I flatpak sperimentali con build notturne di Okular possono essere
    [installati dal repository Flatpak di KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logo di Ark
  name: Sorgenti di rilascio
  text: Okular è rilasciato regolarmente come parte di KDE Gear. Se vuoi generarlo
    dal sorgente, puoi consultare il [paragrafo Compilalo](/build-it).
- image: /images/windows.svg
  image_alt: Logo di Windows
  name: Windows
  text: Per informazioni su come installare software KDE su Windows, dai un'occhiata
    alla [Iniziativa KDE su Windows](https://community.kde.org/Windows). Il rilascio
    stabile è disponibile nel [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Esistono anche [build notturne sperimentali](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    per le quali sono ben accetti test e segnalazioni di errori.
sassFiles:
- /sass/download.scss
title: Scarica
---
