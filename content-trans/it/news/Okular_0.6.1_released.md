---
date: 2008-02-05
title: Rilasciato Okular 0.6.1
---
Il primo rilascio di manutenzione della serie KDE 4.0 comprende Okular 0.6.1. Include pochissime correzioni errori, tra cui il non riscaricamento dei file durante il salvataggio, miglioramenti dell'usabilità, etc. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
