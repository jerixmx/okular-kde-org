---
date: 2009-12-01
title: Rilasciato Okular 0.9.4
---
Il quarto rilascio di manutenzione della serie KDE 4.3, comprende Okular 0.9.4. Sono incluse alcune correzioni dei crash e qualche piccola correzione degli errori nell'interfaccia. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
