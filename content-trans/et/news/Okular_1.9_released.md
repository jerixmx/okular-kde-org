---
date: 2019-12-12
title: Ilmus Okular 1.9
---
Ilmus Okulari versioon 1.9. See väljalase lisab koomiksiarhiivide cb7 toetuse ja pakub lisaks muudele parandustele ja vähema tähendusega võimalustele PDF-failide JavaScripti toetuse täiendusi. Täielikku muutuste logi näeb aadressil <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.9 peale.
