---
date: 2016-08-18
title: Ilmus Okular 0.26
---
Okulari versioon 0.26 ilmus koos KDE rakenduste väljalaskega 16.08. See väljalase sisaldab väga vähe muutusi. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.26 peale.
