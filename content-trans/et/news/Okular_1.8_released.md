---
date: 2019-08-15
title: Ilmus Okular 1.8
---
Okulari versioon 1.8 ilmus koos KDE rakenduste väljalaskega 19.08. See väljalase sisaldab väiksemate paranduste ja uute võimaluste kõrval joonannotatsioonide otste määramise võimalust. Täieliku muudatuste logi leiab aadressil<a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a> ning kõigil, kes kasutavad Okulari, on soovitatav versioon 1.8 kasutusele võtta.
