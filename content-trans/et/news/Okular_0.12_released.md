---
date: 2011-01-26
title: Ilmus Okular 0.12
---
Okulari versioon 0.12 ilmus koos KDE rakenduste väljalaskega 4.6. See väljalase sisaldab väiksemaid parandusi ja uusi võimalusi ning kõigil, kes kasutavad Okulari, on soovitatav see kasutusele võtta.
