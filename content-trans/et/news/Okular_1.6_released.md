---
date: 2018-12-13
title: Ilmus Okular 1.6
---
Okulari versioon 1.6 ilmus koos KDE rakenduste väljalaskega 18.12. See väljalase sisaldab väiksemate paranduste ja uute võimaluste kõrval uut masinakirjas annoteerimise tööriista. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.6 peale.
