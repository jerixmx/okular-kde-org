---
date: 2014-04-16
title: Okular 0.19izdan
---
Verzija okular 0.19 je pušten uz KDE 4.13 izdanje. Ovo izdanje uvodi nove funkcije kao što je kartični interfejs, upotreba DPI ekrana, tako da veličina strana odgovara stvarnoj veličinu papira, poboljšanja u Poništi/Vrati okviri i drugih osobina/ usavršavanja. Okular 0.19 je preporučena nadogradnja za sve koji koriste okular.
