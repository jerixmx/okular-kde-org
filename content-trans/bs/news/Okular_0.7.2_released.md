---
date: 2008-10-03
title: Okular 0.7.2 pušten
---
Drugo izdanje KDE 4.1 serije uključuje Okular 0.7.2. Ono uključuje neke minorne popravke u TIFF i Comicbook datotekama i promjenu na "Prilagodi Širinu" kao zadani nivo povećanja. Sve riješene probleme vezane za ovu temu možete pronaći na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
