---
date: 2009-03-04
title: Okular 0.8.1 pušten
---
Prvo izdanje KDE 4.2 serije uključuje Okular 0.8.1. Ono uključuje neke popravke krahiranja u CHM i DjVu pozadinskim datotekama, i minorne popravke u korisničkom sučelju. Sve riješene probleme vezane za ovu temu možete pročitati na  <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
