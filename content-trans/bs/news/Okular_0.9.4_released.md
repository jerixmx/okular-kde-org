---
date: 2009-12-01
title: Okular 0.9.4 pušten
---
Četvrto izdanje KDE 4.3 serije uključuje Okular 0.9.4. To uključuje i neke ispravke krahiranja, i nekoliko malih popravke u sučelju. Sve riješene probleme vezane za ovu temu možete pročitati  na <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php</a>
