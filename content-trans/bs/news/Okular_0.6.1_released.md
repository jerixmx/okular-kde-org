---
date: 2008-02-05
title: Okular 0.6.1 pušten
---
Prvo izdanje KDE 4.0 serije uključuje Okular 0.6.1. Ono uključuje nekoliko popravljenih grešaka, uključujući bolje ne ponovno preuzimanje datoteke prilikom spremanja, poboljšanje iskoristivosti, itd. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a>
